package com.example.squarejni;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	TextView tv, tv2;
	SquareWapper sqr;
	String text1;
	int []a;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv =(TextView)findViewById(R.id.tv);
		tv2 =(TextView)findViewById(R.id.tv2);
	
		sqr = new SquareWapper(this);
		
		int x = sqr.square(10); // square from JNI
		a = sqr.square2(10); // passing array from JNI
	
		
		for(int i = 0 ; i < 10 ; i ++ )
		{
			text1 +=a[i]+ " : ";
		}
		
		
		
		tv.setText("Square From JNI : "+x);
		tv2.setText("Int Array from JNI:\n"+text1);
	
		
		
		
	}
}
