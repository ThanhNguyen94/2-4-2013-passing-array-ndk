package com.example.squarejni;

import android.content.Context;

public class SquareWapper {

	SquareWapper(Context context)
	{
		context = context;
	}
	
	public static native int square(int base);
	public static native int[] square2(int n);

	
	static
	{
		System.loadLibrary("square");
	}
	
	
	
	
}
